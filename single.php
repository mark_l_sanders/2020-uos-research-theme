<?php  
/** 
 * Single post template file. 
 * 
 * @Package UoS Research
 */ 
get_header(); ?> 
<div class="row">
    <div class="col-md-9">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
    <?php endwhile; else: ?> 
    <?php // If no content, include the "No posts found" template. 
				get_template_part( 'content', 'none' ); ?> 
    <?php endif; ?> 
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
    <div <?php post_class(); ?>> <a href="<?php the_permalink(); ?>" title="<?php the_title();?>"> 
      <h1> 
        <?php the_title();?> 
      </h1> 
      </a> 
      <?php if(get_the_author()!=""){ ?> 
      <p class="author"><i class="fa fa-user"></i> By 
        <?php the_author_posts_link(); ?> 
        <i class="fa fa-calendar"></i> 
        <?php the_time('M.d, Y') ?> 
      </p> 
      <?php } ?> 
      <?php the_post_thumbnail( 'full' ); ?> 
    </div> 
    <?php the_content('<span class="btn btn-primary readmore">Read more.....</span>'); ?> 
    <div> 
      <?php if ( is_home()) : ?> 
      &nbsp; 
      <?php else : ?> 
      <hr class="style"/> 
      <?php the_tags('Tags: ', ', ', '<br />'); ?> 
      Posted in 
      <?php the_category(', ') ?> 
      <hr class="style"/> 
      <?php endif ; ?> 
    </div> 
    <!-- /.post_class --> 
    <?php comments_popup_link('<i class="fa fa-comment-o"></i> 
 Leave a comment', '<i class="fa fa-comment"></i> 
 1 Comment', '<i class="fa fa-comments"></i> 
 % Comments'); ?> 
    <hr class="style"/> 
    <?php endwhile; endif; ?> 
    <?php comments_template(); // Get wp-comments.php template ?> 
    <?php if(get_posts_nav_link()!=""){ ?> 
    <div class="pagination"> 
      <?php wp_link_pages(); ?> 
    </div> 
    <?php } ?> 
  </div> 
  <!-- /.col-md-9 --> 
  <div class="col-md-3 sidebar"> 
    <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?> 
    <div id="secondary" class="widget-area" role="complementary"> 
      <?php dynamic_sidebar( 'sidebar-1' ); ?> 
    </div> 
    <?php endif; ?> 
  </div> 
  <!-- /.col-md-3 sidebar -->  
</div> 
<!-- /.row --> 
<?php get_footer(); ?> 
