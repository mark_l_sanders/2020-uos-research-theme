<?php 
/* 
Template Name: Two column template 
*/ 
get_header(); ?> 
   <div class="row">
    <div class="col-md-12">
          <div class="col-md-9 page-body"> 
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
            <?php endwhile; else: ?> 
            <?php // If no content, include the "No posts found" template. 
				get_template_part( 'content', 'none' ); ?> 
            <?php endif; ?> 
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
            <h1 id="post-<?php the_ID(); ?>"> 
              <?php the_title(); ?> 
            </h1> 
            <?php the_content(); ?> 
            <!-- /.post_class --> 
            <?php endwhile; endif; ?> 
            <?php if ( get_theme_mod( 'uos_research_page_comment_toggle' ) == 1 ) : // show page comments? ?> 
            <?php comments_template(); // Get wp-comments.php template ?> 
             <?php endif; ?> 
          </div> 
          <!-- /.col-md-9 page-body --> 
          <div class="col-md-3 sidebar">
        <?php if ( is_active_sidebar( 'two-column-sidebar' ) ) : ?> 
        <div id="secondary" class="widget-area" role="complementary"> 
          <?php dynamic_sidebar( 'two-column-sidebar' ); ?> 
        </div> 
        <?php endif; ?> 
          <!-- /.col-md-3 sidebar -->  
        </div> 
      </div> 
      <!-- /.col-md-12 -->  
    </div> 
    <!-- /.row --> 
     
 <?php get_footer(); ?>