<?php 
/** 
 * The template for displaying Archive pages 
 * 
 * Used to display archive-type pages if nothing more specific matches a query. 
 * For example, puts together date-based pages if no date.php file exists. 
 * 
 * If you'd like to further customize these archive views, you may create a 
 * new template file for each specific one. For example, Twenty Fourteen 
 * already has tag.php for Tag archives, category.php for Category archives, 
 * and author.php for Author archives. 
 * 
 * @link http://codex.wordpress.org/Template_Hierarchy 
 * 
 * @package UOS-research
 * 
 */ 
  get_header(); ?> 
<div class="container wrapper"> 
  <div class="col-md-12">
    <div class="row">
        <div class="col-md-9">
      <?php if (have_posts()) : ?> 
      <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?> 
      <?php /* If this is a category archive */ if (is_category()) { ?> 
        <h1>Posts in 
	  <?php single_cat_title(); ?>  
        Category  </h1> 
      <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?> 
      <h1>Posts tagged: 
      <?php single_tag_title(); ?> 
      </h1> 
      <?php /* If this is a daily archive */ } elseif (is_day()) { ?> 
      <h1>Archive for  
	  <?php echo get_the_date(); ?> 
      </h1> 
      <?php /* If this is a monthly archive */ } elseif (is_month()) { ?> 
      <h1>Archive for 
      <?php echo get_the_date(); ?> 
      </h1> 
      <?php /* If this is a yearly archive */ } elseif (is_year()) { ?> 
      <h1>Archive for  
	  <?php echo get_the_date(); ?> 
      </h1> 
      <?php /* If this is an author archive */ } elseif (is_author()) {  
			if(isset($_GET['author_name'])) : 
			$curauth = get_userdatabylogin($author_name);  
			else : 
			$curauth = get_userdata(intval($author)); 
			endif; 
			?> 
      <h1>Posts by <?php echo $curauth->display_name; ?></h1> 
      <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?> 
        <h1>Archive</h1> 
        <?php } ?> 
      <?php while (have_posts()) : the_post(); ?> 
      <div class="post"> 
        <h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"> 
          <?php the_title(); ?> 
          </a></h2> 
        <small> 
        <?php get_the_time(); ?> 
        </small> 
        <div class="entry"> 
          <?php the_content('<span class="btn btn-primary readmore">Read more.....</span>'); ?> 
        </div><!--/.entry--> 
        <p class="postmetadata"> 
          <?php the_tags('Tags: ', ', ', '<br />'); ?> 
          <!-- Posted in <?php the_category(', ') ?>  | --> 
          <?php edit_post_link('Edit', '', ' | '); ?> 
          <?php comments_popup_link('Leave a comment', '1 Comment', '% Comments'); ?> 
        </p> 
        <hr class="style">
      </div><!--/.post--> 
      <?php endwhile; ?> 
      <div class="navigation"> 
        <?php next_posts_link('Older Entries') ?> 
        <?php previous_posts_link('Newer Entries') ?> 
      </div><!--/.navigation--> 
      <?php else : ?> 
      <?php endif; ?> 
    </div> 
    <!-- /.col-md-9 --> 
    <div class="col-md-3 sidebar">
      <?php if ( dynamic_sidebar('Home right sidebar ') ) : else : endif; ?> 
    </div> 
    <!-- /.sidebar -->  
  </div><!-- /.row -->  
</div> 
<!-- /.col-md-12 --> 
</div> 
<!-- /.container wrapper --> 
<?php get_footer(); ?> 
