<?php 
/** 
 * The template for displaying the footer. 
 * 
 * Contains the closing of the id=main div and all content after 
 * 
 * @package WordPress 
 * @package New UOS-research
 */ 
?>

</div>

<footer>
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div id="footer-left" class="widget-area" role="complementary">
        <?php dynamic_sidebar( 'footer-left' ); ?>
      </div>
      <!--/.widget-area--> 
    </div>
    <!--/.col-md-4-->
    <div class="col-md-4">
      <div id="footer-middle" class="widget-area" role="complementary">
        <?php dynamic_sidebar( 'footer-middle' ); ?>
      </div>
      <!--/.widget-area--> 
    </div>
    <!--/.col-md-4-->
    <div class="col-md-4">
      <div id="footer-right" class="widget-area" role="complementary">
        <?php dynamic_sidebar( 'footer-right' ); ?>
      </div>
      <!--/.widget-area--> 
    </div>
    <!--/.col-md-4--> 
  </div>
  <!--row-->
  <div class="row">
    <div class="col-md-12">
      <div class="site-info"> <a href="http://www.salford.ac.uk" class="siteFooter-subtext-right"> <span class="siteFooter-subtext-copyright"> &copy; University of Salford </span> </a>
    <?php dynamic_sidebar( 'copyright' ); ?>
  </div>
  <!--/.site-info-->
    </div>
  </div>
</div>
<!--/ .container-fluid-->  
  <?php wp_footer(); ?>
</footer>



<!--/.container wrapper--> 
<script type="text/javascript">
jQuery(document).ready(function($){  
$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) { 
    // Avoid following the href location when clicking 
    event.preventDefault();  
    // Avoid having the menu to close when clicking 
    event.stopPropagation();  
    // If a menu is already open we close it 
    $('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open'); 
    // opening the one you clicked on 
    $(this).parent().addClass('open'); 
   var menu = $(this).parent().find("ul"); 
    var menupos = menu.offset(); 
   
    if ((menupos.left + menu.width()) + 30 > $(window).width()) { 
        var newpos = - menu.width();       
    } else { 
        var newpos = $(this).parent().width(); 
    } 
    menu.css({ left:newpos }); 
}); 
}); 
</script>
</body></html>