<?php 
/** 
 * Displays the searchform of the theme. 
 * 
 * @package uos_research design 
 * @subPackage UoS Research
 */ 
?> 
<form action="<?php echo esc_url( home_url( '/' ) ); ?>"id="search-form" class="searchform clearfix" method="get"> 
	<input type="text" class="search-field" name="s" id="search"  placeholder="Search site..." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search this site...'"/> 
	<input type="submit" value="<?php esc_attr_e( 'Search', 'castaway' ); ?>" id="search-submit" name="submit" class="submit theme-color"> 
</form><!-- .searchform --> 
