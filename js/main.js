var docready=[],$=function(){return{ready:function(fn){docready.push(fn)}}};
var $;
/* foreach polyfill */
        if (!Array.prototype.forEach)
        {
          Array.prototype.forEach = function(fun /*, thisArg */)
          {
            "use strict";

            if (this === void 0 || this === null)
              throw new TypeError();

            var t = Object(this);
            var len = t.length >>> 0;
            if (typeof fun !== "function")
              throw new TypeError();

            var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
            for (var i = 0; i < len; i++)
            {
              if (i in t)
                fun.call(thisArg, t[i], i, t);
            }
          };
        }

Modernizr.load(
  [
    {
      load: '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        }, 
		{
      load: '/js/bootstrap.min.js', 
      complete: function() {
        // fix autoadvance of carousels after manual advance
         $.fn.carousel.defaults = {
          interval: false,
          pause: 'hover'
         }
        
        var scrollToElement = function(selector, time, verticalOffset) {
          time = typeof(time) != 'undefined' ? time : 1000;
          verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
          element = $(selector);
          offset = element.offset();
          offsetTop = offset.top + verticalOffset;
          $('html, body').animate({
            scrollTop: offsetTop
          }, time);
        }
        
        /*var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
        if (!isSafari){
          // Safari seems to get confused with the cookies*/
        var ugcourse = '/ug-courses';
        var pgtcourse = '/pgt-courses';
        if ((window.location.pathname.substring(0, ugcourse.length) === ugcourse) ||
            (window.location.pathname.substring(0, pgtcourse.length) === pgtcourse)){
          var cookies = document.cookie;
          var re = /year=([0-9-]*);*/;
          var curYear = cookies.match(re);
          if (typeof curYear === 'object' && curYear !== null){
            curYear = curYear[1];
            if (document.location.hash == ""){
              window.location.replace(window.location.href+"#tab_year_"+curYear);
            }
          }
        }
        //document.cookie = "year=;expires=expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;domain=salford.ac.uk";
        //}
        // Javascript to enable link to tab/collapse pane
       
        var handleHashchange = function(hash){
          if (hash.substr(1,4)=="tab_"){
            $('.nav-tabs a[href='+hash.replace("tab_","")+']').tab('show');
          } else if (hash.substr(1,5)=="pane_"){
            $(".accordion").each(function(){ 
              $(this).find(".accordion-body").collapse("hide").end()
                     .find(hash.replace("pane_","")).collapse("show"); 
              scrollToElement(hash.replace("pane_", ""));
            });  
          }
        }
       
        handleHashchange(document.location.hash);
              
        $(window).on("hashchange", function(){
          handleHashchange(document.location.hash);
        });       
        
        $('.accordion').on('show', function (e) {
         $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        });

        $('.accordion').on('hide', function (e) {
          $(this).find('.accordion-toggle').not($(e.target)).removeClass('active');
        });

        $('.accordion2').on('show', function (e) {
         $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        });

        $('.accordion2').on('hide', function (e) {
          $(this).find('.accordion-toggle').not($(e.target)).removeClass('active');
        });
      
         var eventpath = $("iframe[src*=eventbrite]").next().find("a:nth-child(3)").attr("href");
        $("iframe[src*=eventbrite]").addClass("hidden-phone hidden-tablet").after("<a href='"+eventpath+"' class='btn btn-large btn-block btn-primary hidden-desktop'>Register Now</a>").siblings("div").addClass("hidden-phone hidden-tablet");
        
        $('.date').each(function(){
          if ($(this).find('.datepart').slice(0,1).html()==$(this).find('.datepart').slice(1,2).html()){
            $(this).find('.datepart').slice(1,2).remove();
          }
          $(this).filter('[data-allday="Yes"]').each(function(){
            $(this).find('.timepart').remove();
            if ($(this).children().last().html()=="-"){
              $(this).children().last().remove();
            }
          })
        });
      }
    }, {
      //test: document.getElementsByClassName('video').length > 0,
      load: ['/__data/assets/js_file/0006/259053/colorboxmin.js' /*'./?a=259053'*/], 
      complete: function() {
        var vidOptions = {
          iframe:true, 
          width: 640,
          height: 480,
          maxWidth:"75%", 
          maxHeight: "75%", 
          transition:"fade",
          close: '&#10006;',
          onOpen: function (){
            $(this).data("href", this.href);
            if ((this.href.indexOf("youtube")>0) && (this.href.indexOf("watch")>0)){
              var re = /\?v\=([\w-_]+)/;
              var vidcode = re.exec(this.href);
              this.href = "http://www.youtube.com/embed/"+vidcode[1]+"?autoplay=1&rel=0";
            }else if (this.href.indexOf("vimeo")>0){
              var re = /([0-9]+)/;  
              var vidcode = re.exec(this.href);
              this.href = "http://player.vimeo.com/video/"+vidcode[1]+"?autoplay=1";  
            }
          },
          onCleanup: function (){
            $(this).attr("href", $(this).data("href"));
          }
        };
        
        add_play = function(target){
          $(target).append("<div class='play'><i class='icon-salplay'/></div>");
        }
        
        youtube_thumbs = function(){
          $(".youtube").each(function(){
              if ((this.href.indexOf("youtube")>0) && (this.href.indexOf("watch")>0)){
                var re = /\?v\=([\w-_]+)/;
                var vidcode = re.exec(this.href);
                var imgsrc = "http://img.youtube.com/vi/"+vidcode[1]+"/0.jpg";
                $(this).attr("data-category", "youtube-video").attr("data-label", vidcode[1]).attr("data-action", "watch video");
                $(this).html("<img src='"+imgsrc+"' class='videothumb' />");
                add_play(this);
              }
            });
        }
        
        vimeo_thumbs = function(){
          $(".vimeo").each(function(){
            var maxwidth = 800;
            if ($(this).hasClass("video-large")){
              maxwidth = 1200;  
            }
            if (this.href.indexOf("vimeo")>0){
              var re = /([0-9]+)/;  
              var vidcode = re.exec(this.href);
              var curElement = $(this);
              $(this).attr("data-vidcode", vidcode[1]);
              var jsonsrc = "http://vimeo.com/api/oembed.json";
              $.ajax({
                url: jsonsrc,
                type: "GET",
                contentType: "application/json"  ,
                dataType: "jsonp",
                data: {
                  url: "http://vimeo.com/"+vidcode[1],
                  maxwidth: maxwidth
                },
                success: function(data){
                  var imgsrc = data.thumbnail_url;
                  var vidcode = data.video_id;
                  curElement.attr("data-category", "vimeo-video").attr("data-label", data.video_id).attr("data-action", "watch video");
                  curElement.html("<img src='"+imgsrc+"' class='videothumb' />");                    
                  add_play(curElement);
                }
              });
            }
          });
        }
        
        var galOptions = {
          rel:'gallery',
          next: '&#9654;', 
          previous: '&#9664;', 
          close: '&#10006;',
          onComplete: function(){
            $("#cboxLoadedContent").append("<div class='caption'>"+$(this).attr("data-caption")+"</div>");
          }
        };



        $mobile_colorbox = function () {
          if ( $(window).width() <= 767 ) {
            $(".video").colorbox.remove();
            $(".cbthumb").colorbox.remove();
            $("#leftsubnav").collapse('hide');
          } else {
            $(".video").colorbox(vidOptions);
            $(".cbthumb").colorbox(galOptions);
            if($("#leftsubnav").attr("style") =="height: 0px;"){
              $("#leftsubnav").collapse('show');
            }
          }            
        }
        
        var timer;
        var has_scrolled;
        
        $(window).scroll(function(){
          has_scrolled = true;
          if (timer) { 
            clearTimeout(timer); 
          }
          
          timer = setTimeout(function(){
            has_scrolled = false;
          }, 1000);
        });
        
        youtube_thumbs();
        vimeo_thumbs();
        $mobile_colorbox();
        $(window).resize(function() {
          if (!has_scrolled){
            $mobile_colorbox();
          }
        });

        var rows = $(".cbgallery > a"); 
        for(var i = 0; i < rows.length; i+=3) { 
          rows.slice(i, i+3).wrapAll("<div class='cbrow'></div>");
        } 
        
        
        $(".news_inline_img").colorbox({
          close: '&#10006;'
        });
        
        $(".mapview").colorbox({
          scrolling: false,
          iframe: true,
          width: "600px",
          height: "470px"
        });
        
        if ($(".accordion").length>0){
          $(".accordion").each(function(index){$(this).attr("id", $(this).attr("id")+index)});
          $(".accordion").each(
            function(accindex){
              $(this).find("a.accordion-toggle").each(
                function(){
                  $(this).attr("href", $(this).attr("href")+"a"+accindex).attr("data-parent", $(this).attr("data-parent")+accindex);
                }
              ).end().find(".accordion-body").each(function(){
                $(this).attr("id", $(this).attr("id")+"a"+accindex);
              })
            }
          )
        }
        
        var oembedproviders = {
          soundcloud: {endpoint: "http://soundcloud.com/oembed", jsonp: "js"},
          slideshare: {endpoint: "http://www.slideshare.net/api/oembed/2", jsonp: "jsonp"},
          storify: {endpoint: "http://api.embed.ly/1/oembed", jsonp: "json"},
          vimeo: {endpoint: "http://vimeo.com/api/oembed.json", jsonp: "jsonp"},
          youtube: {endpoint: "http://api.embed.ly/1/oembed", jsonp: "json"},
          hulu: {endpoint: "http://www.hulu.com/api/oembed.json", jsonp: ""},
          geograph: {endpoint: "http://api.geograph.org.uk/api/oembed", jsonp: "json"},
          instagram: {endpoint: "http://api.instagram.com/oembed", jsonp: "json"},
          scribd: {endpoint: "http://api.embed.ly/1/oembed", jsonp: "json"},
          twitter: {endpoint: "https://api.twitter.com/1/statuses/oembed.json", jsonp: "json"},
          prezi: {endpoint: "http://api.embed.ly/1/oembed", jsonp: "json"}
        };
                
        oembed = function(){
          $(".oembed").each(function(){
            var curWidth = $(this).parent().width();
            var curElement = this;
            var curClasses = this.className.split(/\s+/);
            curClasses.forEach(function(element, index, array){
              if (oembedproviders[element] != undefined){
                $.ajax({
                  url: oembedproviders[element]['endpoint'],
                  type: "GET",
                  contentType: "application/json"  ,
                  dataType: "jsonp",
                  data: {
                    url: $(curElement).attr("href"),
                    maxwidth: curWidth,
                    width: curWidth,
                    format: oembedproviders[element]['jsonp']
                  },
                  success: function(data){
                    if (data.type=="photo"){
                      $(curElement).html("<img src='"+data.url+"' alt='"+data.description+"' title='"+data.title+"' />");
                    } else if (data.type=="link"){
                      $(curElement).html("<iframe href='"+data.url+"' style='width:"+data.width+"; height:"+data.height+"'></iframe>");
                    }else {
                      $(curElement).html(data.html);
                    }
                  }
                });
              }
            });
            
          });
        }
        
        oembed();
        //window.addEventListener("orientationchange", oembed, false);
        $(window).on("orientationchange", oembed);

        if ($("[data-category]").length>0){
          $("[data-category]").on("click", function(){
            category = $(this).attr("data-category");
            action = $(this).attr("data-action");
            label = $(this).attr("data-label");
            _sz.push(['event', category, action, label]);
            ga("send", "event", category, action, label);
          });
        }
        
        if ($('a[href^="http"]').length>0){
          $('a[href^="http"]').not('[href^="http://www.salford.ac.uk"]').not("[data-category]").on("click", function(){
            category = "External link";
            action = "Click";
            label = $(this).attr("href");
            _sz.push(['event', category, action, label]);
            ga("send", "event", category, action, label);
          });
        }
        
        $('.nav-tabs').on('click','[data-toggle="tab"]:last-child',function (e) {
          var curPath = e.currentTarget.href;
          curPath = curPath.substring(curPath.indexOf('/',8));
          var curHash = curPath.substring(curPath.indexOf('#'));
          curPath=curPath.split('#')[0];
          _sz.push(['event', 'Courses', curPath, curHash]);
          ga("send", "event", 'Courses', curPath, curHash);
        });
      }
    }, {
      test: Modernizr.mq('only all'),
      nope: ['./?a=242113', './?a=250956']
    }, {
      test: Modernizr.input.placeholder,
      nope: ['./?a=303935'],
      callback: function(){
        $.placeholder.shim();  
      }
    }, {
      test: Modernizr.touch,
      yep: ['./?a=329277']  
    }, {
      load: ['https://j.flxpxl.com/51781.js']  
    }
  ]
);