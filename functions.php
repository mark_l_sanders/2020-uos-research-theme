<?php 
/*
 * functions.php
 * You should use child-themes for customizing bootstrapped theme
 */
 
// Register Custom Navigation Walker
require_once('inc/wp_bootstrap_navwalker.php');
 
//Remove HTML guide under comments
add_filter( 'comment_form_defaults', 'remove_comment_tags' );
function remove_comment_tags( $defaults ) {
    $defaults['comment_notes_after'] = '';
    return $defaults;
}

//Add custom header support
add_theme_support( 'custom-header' );

//add menu support


// Add the new menu
register_nav_menus( array(
	'header-menu' => __( 'Main Menu', 'uos_research' ),
	'secondary' => __( 'Secondary menu', 'uos_research'),
	'tertiary' => __( 'Tertiary menu', 'uos_research'),
	'footer' => __( 'Footer menu', 'uos_research'),
) );


//Add automatic feed links
add_theme_support( 'automatic-feed-links' );

//Add post thumbnails
if ( function_exists( 'add_theme_support' ) ) {
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 150, 150, true );
}

//Add editor style to theme
function my_theme_add_editor_styles() {
    add_editor_style( 'inc/custom-editor-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );

//Add max content width
if ( ! isset( $content_width ) ) {
	$content_width = 960;
}

//Add breadcrumb.php
require_once(get_template_directory().'/inc/breadcrumbs.php');

//Add comment reply script
if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

//Start theme customizer
function uos_research_theme_customizer( $wp_customize ) {

// include other function files 
require_once(get_template_directory().'/inc/bootstrap-carousel.php');

//Add custom logo to design
	$wp_customize->add_section( 'uos_research_logo_section' , array(
		'title'      => __( 'Logo', 'uos_research' ),
		'priority'   => 30,
	) );

$wp_customize->add_setting( 'uos_research_logo' );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'uos_research_logo', array(
    'label'    => __( 'Logo', 'uos_research' ),
    'description' => 'Upload a logo to go next to the site name and description in the header (200x60px)',
    'section'  => 'uos_research_logo_section',
    'settings' => 'uos_research_logo',
) ) );
	$wp_customize->add_setting( 'uos_research_university_logo', array( 
		'default' => 0,
		'sanitize_callback' => 'research_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'uos_research_university_logo', array(
		'label'     => __( 'Remove University of Salford Logo?', 'uos_research' ),
		'section'   => 'uos_research_logo_section',
		'priority'  => 0,
		'type'      => 'checkbox',
    'settings' => 'uos_research_university_logo',
	) );




// add "Theme Options" section
	$wp_customize->add_section( 'uos_research_content_options_section' , array(
		'title'      => __( 'Theme Options', 'uos_research' ),
		'priority'   => 100,
	) );
	
// add setting for comment toggle checkbox
	$wp_customize->add_setting( 'uos_research_page_comment_toggle', array( 
		'default' => 1 ,
		'sanitize_callback' => 'bootstrapped_sanitize_checkbox',
	) );
	
	
// add control for toggle checkbox
	$wp_customize->add_control( 'uos_research_page_comment_toggle', array(
		'label'     => __( 'Show comments on pages?', 'uos_research' ),
		'section'   => 'uos_research_content_options_section',
		'priority'  => 10,
		'type'      => 'checkbox'
	) );
	

// add setting for breadcrumb toggle checkbox
	$wp_customize->add_setting( 'uos_research_page_breadcrumb_toggle', array( 
		'default' => 1 ,
		'sanitize_callback' => 'bootstrapped_sanitize_checkbox',
	) );
	
// add control for toggle checkbox
	$wp_customize->add_control( 'uos_research_page_breadcrumb_toggle', array(
		'label'     => __( 'Show breadcrumbs on site?', 'uos_research' ),
		'section'   => 'uos_research_content_options_section',
		'priority'  => 11,
		'type'      => 'checkbox'
	) );
// add setting for social media icons toggle checkbox 
	$wp_customize->add_setting( 'uos_research_page_social_icons_toggle', array(  
		'default' => 0 , 
		'sanitize_callback' => 'bootstrapped_sanitize_checkbox', 
	) ); 
	 
// add control for social media icons toggle checkbox 
	$wp_customize->add_control( 'uos_research_page_social_icons_toggle', array( 
		'label'     => __( 'Show social media icons', 'uos_research' ), 
		'section'   => 'uos_research_content_options_section', 
		'priority'  => 12, 
		'type'      => 'checkbox' 
	) );	

}
add_action('customize_register', 'uos_research_theme_customizer');

// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);


// Load javascript for theme
function uos_researchbootstrap_scripts_with_jquery()
{
	// Register the script for this theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'uos_researchbootstrap_scripts_with_jquery' );

//Add custom sidebars
function uos_research_widgets_init() {

	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'uos_research' ),
		'id' => 'sidebar-1',
		'description' => __( 'The main sidebar appears on the right on each page except the front page template', 'uos_research' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title theme-text-color">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' =>__( 'Front page sidebar', 'uos_research'),
		'id' => 'sidebar-2',
		'description' => __( 'Appears on the static front page template', 'uos_research' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title theme-text-color">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' =>__( 'Footer left', 'uos_research'),
		'id' => 'footer-left',
		'description' => __( 'Appears on the left footer', 'uos_research' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title theme-text-color">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' =>__( 'Footer middle', 'uos_research'),
		'id' => 'footer-middle',
		'description' => __( 'Appears on the middle footer', 'uos_research' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title theme-text-color">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' =>__( 'Footer right', 'uos_research'),
		'id' => 'footer-right',
		'description' => __( 'Appears on the right footer', 'uos_research' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title theme-text-color">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' =>__( 'Copyright', 'uos_research'),
		'id' => 'copyright',
		'description' => __( 'Appears on footer', 'uos_research' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title theme-text-color">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' =>__( 'Two column sidebar', 'uos_research'),
		'id' => 'two-column-sidebar',
		'description' => __( 'Appears on two column template', 'uos_research' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title theme-text-color">',
		'after_title' => '</h3>',
	) );
	
	}

add_action( 'widgets_init', 'uos_research_widgets_init' );


/*
 * Function To Load Font Awesome To WordPress
 */

add_action( 'wp_enqueue_scripts', 'uos_research_enqueue_font' );
function uos_research_enqueue_font() {
	if ( !is_admin() ) {
		wp_enqueue_style( 'uos_research_enqueue_font', get_bloginfo( 'stylesheet_directory' ) . '/css/font-awesome.min.css', array(), '4.2' );
	}
}

// Enqueue Scripts/Styles for our Lightbox
function uos_research_add_lightbox() {
    wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/inc/lightbox/js/jquery.fancybox.pack.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'lightbox', get_template_directory_uri() . '/inc/lightbox/js/lightbox.js', array( 'fancybox' ), false, true );
    wp_enqueue_style( 'lightbox-style', get_template_directory_uri() . '/inc/lightbox/css/jquery.fancybox.css' );
	wp_enqueue_style( 'replicapro', get_template_directory_uri() . '/css/font.css');
}
add_action( 'wp_enqueue_scripts', 'uos_research_add_lightbox' );


//Text entry sanitization
function bootstrapped_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}
//Checkbox sanitization
function bootstrapped_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

function research_sanitize_checkbox( $checked ) {
  return ( ( isset( $checked ) && true == $checked ) ? true : false );
}
?>
