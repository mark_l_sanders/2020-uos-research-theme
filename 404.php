<?php  
function castaway_strip_attachments($where) { 
	$where .= ' AND post_type != "attachment"'; 
	return $where; 
} 
add_filter('posts_where','castaway_strip_attachments'); 
get_header(); 
?> 
  
        <h1>Can't find that page, sorry... (Error 404)</h1> 
        <p>Let me help you find what you came here for:</p> 
        <?php  
			$s = preg_replace("/(.*)-(html|htm|php|asp|aspx)$/","$1",$wp_query->query_vars['name']); 
			$posts = query_posts('post_type=any&name='.$s); 
			$s = str_replace("-"," ",$s); 
			if (count($posts) == 0) { 
				$posts = query_posts('post_type=any&s='.$s); 
			} 
			if (count($posts) > 0) { 
				echo "<ol><li>"; 
				echo "<p>Were you looking for <strong>one of the following</strong> posts or pages?</p>"; 
				echo "<ul>"; 
				foreach ($posts as $post) { 
					echo '<li><a href="'.get_permalink($post->ID).'">'.$post->post_title.'</a></li>'; 
				} 
				echo "</ul>"; 
				echo "<p>If not, don't worry, I've got a few more tips for you to find it:</p></li>"; 
			} else { 
				echo "<p><strong>Don't worry though!</strong> I've got a few tips for you to find it:</p>"; 
				echo "<ol>"; 
			} 
		?> 
        <li> 
          <label for="s"><strong>Search</strong> for it:</label> 
          <form style="display:inline;" action="<?php bloginfo('siteurl');?>"> 
            <input type="text" value="<?php echo esc_attr($s); ?>" id="s" name="s"/> 
            <input type="submit" value="Search"/> 
          </form> 
        </li> 
        <li> <strong>If you typed in a URL...</strong> please make sure you check the spelling. Then try reloading the page. </li> 
        <li><strong>Start over again</strong> at the <a href="<?php bloginfo('siteurl');?>">homepage</a>. </li> 
        </ol> 
    </div> 
 
   
<?php get_footer(); ?> 
