  <section id="top-wrapper">
    <div class="container top-container"> 
  <div class="social-media"> 
     
        <?php     	 
	if (has_nav_menu('secondary')) {   
  wp_nav_menu( array(   
        'theme_location' => 'secondary',   
        'menu'       => 'Secondary menu',    
        'depth'      => 1,    
        'container'  => false,    
        'menu_class' => 'list-inline pull-left hidden-xs',    
        'fallback_cb' => 'wp_page_menu',    
        //Process nav menu using custom nav walker    
       	'walker'	=> new wp_bootstrap_navwalker,   
  ) );   
}      
?>    
      <?php $twitter = get_theme_mod( 'twitter' ); if( !empty( $twitter ) ): ?> 
      <a class="twitter" href="<?php echo  esc_url( $twitter ); ?>" title="Follow us on Twitter"><i class="fa fa-twitter"></i></a> 
      <?php endif; ?> 
      <?php $facebook = get_theme_mod( 'facebook' ); if( !empty( $facebook ) ): ?> 
      <a class="facebook" href="<?php echo esc_url( $facebook); ?>" title="Join our Facebook page"><i class="fa fa-facebook"></i></a> 
      <?php endif; ?> 
      <?php $google = get_theme_mod( 'google' ); if( !empty( $google ) ): ?> 
      <a class="google" href="<?php echo  esc_url( $google ); ?>" title="GooglePlus"><i class="fa fa-google-plus"></i> </a> 
      <?php endif; ?> 
      <?php $youtube = get_theme_mod( 'youtube' ); if( !empty( $youtube ) ): ?> 
      <a class="youtube" href="<?php echo  esc_url( $youtube ); ?>" title="Youtube"><i class="fa fa-youtube"></i> </a> 
      <?php endif; ?> 
      <?php $vimeo = get_theme_mod( 'vimeo' ); if( !empty( $vimeo ) ): ?> 
      <a class="vimeo" href="<?php echo  esc_url( $vimeo ); ?>" title="Vimeo"><i class="fa fa-vimeo-square"></i> </a> 
      <?php endif; ?> 
      <?php $pinterest = get_theme_mod( 'pinterest' ); if( !empty( $pinterest ) ): ?> 
      <a class="pinterest" href="<?php echo  esc_url( $pinterest ); ?>" title="Pinterest"><i class="fa fa-pinterest"></i> </a> 
      <?php endif; ?> 
      <?php $instagram = get_theme_mod( 'instagram' ); if( !empty( $instagram ) ): ?> 
      <a class="instagram" href="<?php echo  esc_url( $instagram ); ?>" title="Instagram"><i class="fa fa-instagram"></i> </a> 
      <?php endif; ?> 
      <?php $linkedin = get_theme_mod( 'linkedin' ); if( !empty( $linkedin ) ): ?> 
      <a class="linkedin" href="<?php echo  esc_url( $linkedin ); ?>" title="Linkedin"><i class="fa fa-linkedin"></i> </a> 
      <?php endif; ?> 
         <?php $dribble = get_theme_mod( 'dribble' ); if( !empty( $dribble ) ): ?> 
      <a class="dribble" href="<?php echo  esc_url( $dribble ); ?>" title="Dribble"><i class="fa fa-dribbble"></i> </a> 
      <?php endif; ?> 
         </div> 
    <!--/.social-media-->  
     </div><!--./container-->
</section>