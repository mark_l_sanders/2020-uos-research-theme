<?php 
/** 
 * The carousel for our theme 
 * 
 * @Package UoS Research
 */ 
?>
<?php if(get_theme_mod('first_slide')!=""){ ?>

<div id="myCarousel" class="carousel slide">
  <ol class="carousel-indicators">
    <li data-slide-to="0" data-target="#myCarousel" class="active"></li>
    <?php if(get_theme_mod('second_slide')!=""){ ?>
    <li data-slide-to="1" data-target="#myCarousel" class=""></li>
    <?php } ?>
    <?php if(get_theme_mod('third_slide')!=""){ ?>
    <li data-slide-to="2" data-target="#myCarousel" class=""></li>
    <?php } ?>
    <?php if(get_theme_mod('fourth_slide')!=""){ ?>
    <li data-target="#carousel-captions" data-slide-to="3" class=""></li>
    <?php } ?>
    <?php if(get_theme_mod('fifth_slide')!=""){ ?>
    <li data-target="#carousel-captions" data-slide-to="4" class=""></li>
    <?php } ?>
  </ol>
  <div class="carousel-inner">
    <div class="item active"> <img src="<?php echo get_theme_mod('first_slide') ?>"  alt="<?php echo get_theme_mod('first_slide_title') ?>">

      <div class="carousel-caption">
        <div class="col-md-9">
          <h2><?php echo get_theme_mod('first_slide_title') ?></h2>
          <p><?php echo get_theme_mod('first_slide_caption') ?></p>
        </div>
        <div class="col-md-3"> <a class="btn btn-primary" href="<?php echo get_theme_mod('first_slide_link') ?>"><?php echo get_theme_mod('first_slide_link_title') ?></a> </div>
      </div>
    </div>
    <!-- item active -->
    <?php if(get_theme_mod('second_slide')!=""){ ?>
    <div class="item"> <img src="<?php echo get_theme_mod('second_slide') ?>" alt="<?php echo get_theme_mod('second_slide_title') ?>">

      <div class="carousel-caption">
        <div class="col-md-9">
          <h2><?php echo get_theme_mod('second_slide_title') ?></h2>
          <p><?php echo get_theme_mod('second_slide_caption') ?></p>
        </div>
        <div class="col-md-3"> <a class="btn btn-primary" href="<?php echo get_theme_mod('second_slide_link') ?>"><?php echo get_theme_mod('second_slide_link_title') ?></a> </div>
      </div>
    </div>
    <!-- item -->
    <?php } ?>
    <?php if(get_theme_mod('third_slide')!=""){ ?>
    <div class="item"> <img src="<?php echo get_theme_mod('third_slide') ?>" alt="<?php echo get_theme_mod('third_slide_title') ?>">
      <div class="carousel-caption">
        <div class="col-md-9">
          <h2><?php echo get_theme_mod('third_slide_title') ?></h2>
          <p><?php echo get_theme_mod('third_slide_caption') ?></p>
        </div>
        <div class="col-md-3"> <a class="btn btn-primary" href="<?php echo get_theme_mod('third_slide_link') ?>"><?php echo get_theme_mod('third_slide_link_title') ?></a> </div>
      </div>
    </div>
    <!-- item -->
    <?php } ?>
    <?php if(get_theme_mod('fourth_slide')!=""){ ?>
    <div class="item"> <img src="<?php echo get_theme_mod('fourth_slide') ?>" alt="<?php echo get_theme_mod('fourth_slide_title') ?>">

      <div class="carousel-caption">
        <div class="col-md-9">
          <h2><?php echo get_theme_mod('fourth_slide_title') ?></h2>
          <p><?php echo get_theme_mod('fourth_slide_caption') ?></p>
        </div>
        <div class="col-md-3"> <a class="btn btn-primary" href="<?php echo get_theme_mod('fourth_slide_link') ?>"><?php echo get_theme_mod('fourth_slide_link_title') ?></a> </div>
      </div>
    </div>
    <!-- item -->
    <?php } ?>
    <?php if(get_theme_mod('fifth_slide')!=""){ ?>
    <div class="item"> <img src="<?php echo get_theme_mod('fifth_slide') ?>" alt="<?php echo get_theme_mod('fifth_slide_title') ?>">

      <div class="carousel-caption">
        <div class="col-md-9">
          <h2><?php echo get_theme_mod('fifth_slide_title') ?></h2>
          <p><?php echo get_theme_mod('fifth_slide_caption') ?></p>
        </div>
        <div class="col-md-3"> <a class="btn btn-primary" href="<?php echo get_theme_mod('fifth_slide_link') ?>"><?php echo get_theme_mod('fifth_slide_link_title') ?></a> </div>
      </div>
    </div>
    <!-- item -->
    <?php } ?>
  </div>
  <!-- carousel-inner -->
  <?php if(get_theme_mod('second_slide')!=""){ ?>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a> <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
  <?php } ?>
</div>
<!-- #myCarousel -->

<?php } else { ?>
<?php echo '<img src="' . get_template_directory_uri() . '/img/default-header-image.jpg' . '" alt="" />';?>
<?php } ?>
