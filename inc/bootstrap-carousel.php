<?php

//Wordpress customizer controls for the bootstrap carousel

//add carousel support

$wp_customize->add_section( 'uos_research_carousel', array(
    'title'          => 'Homepage carousel',
    'priority'       => 25,
) );
//First slide settings

$wp_customize->add_setting( 'uos_research_page_carousel_toggle', array( 
		 
		'default' => 1 ,
		'priority'       => 0,
		'sanitize_callback' => 'bootstrapped_sanitize_checkbox',
	) );

$wp_customize->add_control( 'uos_research_page_carousel_toggle', array(
		'label'     => __( 'Tick this box to remove the carousel from the homepage', 'uos_research' ),
		'section'   => 'uos_research_carousel',
		'priority'  => 0,
		'type'      => 'checkbox'
	) );
	
$wp_customize->add_setting( 'uos_research_auto_carousel_toggle', array( 
		 
		'default' => 1 ,
		'priority'       => 0,
		'sanitize_callback' => 'bootstrapped_sanitize_checkbox',
	) );

$wp_customize->add_control( 'uos_research_auto_carousel_toggle', array(
		'label'     => __( 'Tick this box to stop the carousel from auto sliding on page load', 'uos_research' ),
		'section'   => 'uos_research_carousel',
		'priority'  => 0,
		'type'      => 'checkbox'
	) );	

$wp_customize->add_setting( 'first_slide', array(
    'default'        => '',
	 'priority'       => 1,
) );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'first_slide', array(
    'label'   => 'First Slide',
    'section' => 'uos_research_carousel',
    'settings'   => 'first_slide',
	 'priority'       => 1,
) ) );
$wp_customize->add_setting( 'first_slide_title', array(
    'default'        => 'Add title of slide',
	 'priority'       => 2,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'first_slide_title', array(
    'label'   => 'First Slide Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'first_slide_title',
	'type' => 'text',
	 'priority'       => 2,
) ) ;
$wp_customize->add_setting( 'first_slide_caption', array(
    'default'        => 'Add caption of slide',
	 'priority'       => 3,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
	
) );
$wp_customize->add_control( 'first_slide_caption', array(
    'label'   => 'First Slide Caption',
    'section' => 'uos_research_carousel',
    'settings'   => 'first_slide_caption',
	'type' => 'text',
	 'priority'       => 3,
) ) ;
$wp_customize->add_setting( 'first_slide_link', array(
    'default'        => 'http://www.example.com',
	 'priority'       => 4,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'first_slide_link', array(
    'label'   => 'First Slide Link',
    'section' => 'uos_research_carousel',
    'settings'   => 'first_slide_link',
	'type' => 'text',
	 'priority'       => 4,
) ) ;

$wp_customize->add_setting( 'first_slide_link_title', array(
    'default'        => 'Add engaging link title',
	 'priority'       => 5,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'first_slide_link_title', array(
    'label'   => 'First Slide Link Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'first_slide_link_title',
	'type' => 'text',
	 'priority'       => 5,
) ) ;

//Second slide settings
$wp_customize->add_setting( 'second_slide', array(
    'default'        => '',
	 'priority'       => 6,
) );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'second_slide', array(
    'label'   => 'Second Slide',
    'section' => 'uos_research_carousel',
    'settings'   => 'second_slide',
	 'priority'       => 7,
) ) );

$wp_customize->add_setting( 'second_slide_title', array(
    'default'        => 'Add title of slide',
	 'priority'       => 8,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );

$wp_customize->add_control( 'second_slide_title', array(
    'label'   => 'Second Slide Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'second_slide_title',
	'type' => 'text',
	 'priority'       => 8,
) ) ;
$wp_customize->add_setting( 'second_slide_caption', array(
    'default'        => 'Add caption of slide',
	 'priority'       => 9,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'second_slide_caption', array(
    'label'   => 'Second Slide Caption',
    'section' => 'uos_research_carousel',
    'settings'   => 'second_slide_caption',
	'type' => 'text',
	 'priority'       => 9,
) ) ;
$wp_customize->add_setting( 'second_slide_link', array(
    'default'        => 'http://www.example.com',
	 'priority'       => 10,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );

$wp_customize->add_control( 'second_slide_link', array(
    'label'   => 'Second Slide Link',
    'section' => 'uos_research_carousel',
    'settings'   => 'second_slide_link',
	'type' => 'text',
	 'priority'       => 10,
) ) ;
$wp_customize->add_setting( 'second_slide_link_title', array(
    'default'        => 'Add engaging link title',
	 'priority'       => 11,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'second_slide_link_title', array(
    'label'   => 'Second Slide Link Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'second_slide_link_title',
	'type' => 'text',
	 'priority'       => 11,
) ) ;

//Third slide settings
$wp_customize->add_setting( 'third_slide', array(
    'default'        => '',
	 'priority'       => 12,
) );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'third_slide', array(
    'label'   => 'Third Slide',
    'section' => 'uos_research_carousel',
    'settings'   => 'third_slide',
	 'priority'       => 13,
) ) );

$wp_customize->add_setting( 'third_slide_title', array(
    'default'        => 'Add title of slide',
	 'priority'       => 14,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'third_slide_title', array(
    'label'   => 'Third Slide Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'third_slide_title',
	'type' => 'text',
	 'priority'       => 14,
) ) ;
$wp_customize->add_setting( 'third_slide_caption', array(
    'default'        => 'Add caption of slide',
	 'priority'       => 15,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'third_slide_caption', array(
    'label'   => 'Third Slide Caption',
    'section' => 'uos_research_carousel',
    'settings'   => 'third_slide_caption',
	'type' => 'text',
	 'priority'       => 15,
) ) ;
$wp_customize->add_setting( 'third_slide_link', array(
    'default'        => 'http://www.example.com',
	 'priority'       => 16,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );

$wp_customize->add_control( 'third_slide_link', array(
    'label'   => 'Third Slide Link',
    'section' => 'uos_research_carousel',
    'settings'   => 'third_slide_link',
	'type' => 'text',
	 'priority'       => 16,
) ) ;

$wp_customize->add_setting( 'third_slide_link_title', array(
    'default'        => 'Add engaging link title',
	 'priority'       => 17,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'third_slide_link_title', array(
    'label'   => 'Third Slide Link Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'third_slide_link_title',
	'type' => 'text',
	 'priority'       => 17,
) ) ;

//Fourth slide settings
$wp_customize->add_setting( 'fourth_slide', array(
    'default'        => '',
	 'priority'       => 18,
) );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'fourth_slide', array(
    'label'   => 'Fourth Slide',
    'section' => 'uos_research_carousel',
    'settings'   => 'fourth_slide',
	 'priority'       => 19,
) ) );

$wp_customize->add_setting( 'fourth_slide_title', array(
    'default'        => 'Add title of slide',
	 'priority'       => 20,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'fourth_slide_title', array(
    'label'   => 'Fourth Slide Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'fourth_slide_title',
	'type' => 'text',
	 'priority'       => 20,
) ) ;
$wp_customize->add_setting( 'fourth_slide_caption', array(
    'default'        => 'Add caption of slide',
	 'priority'       => 21,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'fourth_slide_caption', array(
    'label'   => 'Fourth Slide Caption',
    'section' => 'uos_research_carousel',
    'settings'   => 'fourth_slide_caption',
	'type' => 'text',
	 'priority'       => 21,
) ) ;
$wp_customize->add_setting( 'fourth_slide_link', array(
    'default'        => 'http://www.example.com',
	 'priority'       => 22,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );

$wp_customize->add_control( 'fourth_slide_link', array(
    'label'   => 'Fourth Slide Link',
    'section' => 'uos_research_carousel',
    'settings'   => 'fourth_slide_link',
	'type' => 'text',
	 'priority'       => 22,
) ) ;
$wp_customize->add_setting( 'fourth_slide_link_title', array(
    'default'        => 'Add engaging link title',
	 'priority'       => 23,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'fourth_slide_link_title', array(
    'label'   => 'fourth Slide Link Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'fourth_slide_link_title',
	'type' => 'text',
	 'priority'       => 23,
) ) ;


//Fifth slide settings
$wp_customize->add_setting( 'fifth_slide', array(
    'default'        => '',
	 'priority'       => 24,
) );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'fifth_slide', array(
    'label'   => 'Fifth Slide',
    'section' => 'uos_research_carousel',
    'settings'   => 'fifth_slide',
	 'priority'       => 25,
) ) );

$wp_customize->add_setting( 'fifth_slide_title', array(
    'default'        => 'Add title of slide',
	 'priority'       => 26,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'fifth_slide_title', array(
    'label'   => 'Fifth Slide Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'fifth_slide_title',
	'type' => 'text',
	 'priority'       => 26,
) ) ;
$wp_customize->add_setting( 'fifth_slide_caption', array(
    'default'        => 'Add caption of slide',
	 'priority'       => 27,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'fifth_slide_caption', array(
    'label'   => 'Fifth Slide Caption',
    'section' => 'uos_research_carousel',
    'settings'   => 'fifth_slide_caption',
	'type' => 'text',
	 'priority'       => 27,
) ) ;
$wp_customize->add_setting( 'fifth_slide_link', array(
    'default'        => 'http://www.example.com',
	 'priority'       => 28,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );

$wp_customize->add_control( 'fifth_slide_link', array(
    'label'   => 'Fifth Slide Link',
    'section' => 'uos_research_carousel',
    'settings'   => 'fifth_slide_link',
	'type' => 'text',
	 'priority'       => 28,
) ) ;
$wp_customize->add_setting( 'fifth_slide_link_title', array(
    'default'        => 'Add engaging link title',
	 'priority'       => 29,
	 'sanitize_callback' => 'bootstrapped_sanitize_text',
) );
$wp_customize->add_control( 'fifth_slide_link_title', array(
    'label'   => 'Fifth Slide Link Title',
    'section' => 'uos_research_carousel',
    'settings'   => 'fifth_slide_link_title',
	'type' => 'text',
	 'priority'       => 29,
) ) ;
