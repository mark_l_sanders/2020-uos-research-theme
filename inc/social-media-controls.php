<?php 
// Add Social Media Section 
$wp_customize->add_section( 'uos_research_social-media' , array( 
    'title' => __( 'Social Media', 'castaway' ), 
    'priority' => 30, 
    'description' => __( 'Enter the URL to your account for each service for the icon to appear in the header.', 'castaway' ) 
) ); 
// Add Twitter Setting 
$wp_customize->add_setting( 'twitter' , array( 'default' => 'Add Twitter URL', 'sanitize_callback' => 'bootstrapped_sanitize_text', )); 
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter', array( 
    'label' => __( 'Twitter', 'castaway' ), 
    'section' => 'uos_research_social-media', 
    'settings' => 'twitter', 
) ) ); 
// Add Facebook Setting 
$wp_customize->add_setting( 'facebook' , array( 'default' => 'Add Facebook URL', 'sanitize_callback' => 'bootstrapped_sanitize_text', )); 
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook', array( 
    'label' => __( 'Facebook', 'castaway' ), 
    'section' => 'uos_research_social-media', 
    'settings' => 'facebook', 
) ) ); 
// Add Google+ Setting 
$wp_customize->add_setting( 'google' , array( 'default' => 'Add Google+ URL', 'sanitize_callback' => 'bootstrapped_sanitize_text', )); 
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'google', array( 
    'label' => __( 'Google +', 'castaway' ), 
    'section' => 'uos_research_social-media', 
    'settings' => 'google', 
) ) ); 
// Add YouTube Setting 
$wp_customize->add_setting( 'youtube' , array( 'default' => 'Add YouTube URL', 'sanitize_callback' => 'bootstrapped_sanitize_text', )); 
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'youtube', array( 
    'label' => __( 'YouTube', 'castaway' ), 
    'section' => 'uos_research_social-media', 
    'settings' => 'youtube', 
) ) ); 
// Add Vimeo Setting 
$wp_customize->add_setting( 'vimeo' , array( 'default' => 'Add Vimeo URL' ,'sanitize_callback' => 'bootstrapped_sanitize_text',)); 
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'vimeo', array( 
    'label' => __( 'Vimeo', 'castaway' ), 
    'section' => 'uos_research_social-media', 
    'settings' => 'vimeo', 
) ) ); 
// Add Instagram Setting 
$wp_customize->add_setting( 'instagram' , array( 'default' => 'Add Instagram URL','sanitize_callback' => 'bootstrapped_sanitize_text', )); 
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'instagram', array( 
    'label' => __( 'Instagram', 'castaway' ), 
    'section' => 'uos_research_social-media', 
    'settings' => 'instagram', 
) ) ); 
// Add Pinterest Setting 
$wp_customize->add_setting( 'pinterest' , array( 'default' => 'Add Pinterest URL' ,'sanitize_callback' => 'bootstrapped_sanitize_text',)); 
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'pinterest', array( 
    'label' => __( 'Pinterest', 'castaway' ), 
    'section' => 'uos_research_social-media', 
    'settings' => 'pinterest', 
) ) ); 
// Add Linkedin Setting 
$wp_customize->add_setting( 'linkedin' , array( 'default' => 'Add Linkedin URL','sanitize_callback' => 'bootstrapped_sanitize_text', )); 
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'linkedin', array( 
    'label' => __( 'Linkedin', 'castaway' ), 
    'section' => 'uos_research_social-media', 
    'settings' => 'linkedin', 
) ) ); 
// Add Dribble Setting 
$wp_customize->add_setting( 'dribble' , array( 'default' => 'Add Dribble URL','sanitize_callback' => 'bootstrapped_sanitize_text', )); 
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'dribble', array( 
    'label' => __( 'Linkedin', 'castaway' ), 
    'section' => 'uos_research_social-media', 
    'settings' => 'dribble', 
) ) ); 
