<?php 
function the_breadcrumb(){  
    $delimiter = '<i class="fa fa-chevron-circle-right"></i>'; 
    $name = 'Home'; 
    $currentBefore = '<span class="current">'; 
    $currentAfter = '</span>'; 
  
    if(!is_home() && !is_front_page() || is_paged()){ 
  
        global $post; 
		echo '<div class="pull-left breadcrumb">'; 
        $home = get_bloginfo('url'); 
        echo '<a href="' . $home . '"><span class="fa fa-home"></span></a> ' . $delimiter . ' '; 
  
        if(is_tax()){ 
              $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy')); 
              echo $currentBefore . $term->name . $currentAfter; 
			  echo '</div>'; 
			  
  
        } elseif (is_category()){ 
            global $wp_query; 
            $cat_obj = $wp_query->get_queried_object(); 
            $thisCat = $cat_obj->term_id; 
            $thisCat = get_category($thisCat); 
            $parentCat = get_category($thisCat->parent); 
            if($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ')); 
            echo $currentBefore . ''; 
            single_cat_title(); 
            echo '' . $currentAfter; 
			echo '</div>'; 
  
        } elseif (is_day()){ 
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' '; 
            echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' '; 
            echo $currentBefore . get_the_time('d') . $currentAfter; 
			echo '</div>'; 
  
        } elseif (is_month()){ 
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' '; 
            echo $currentBefore . get_the_time('F') . $currentAfter; 
			echo '</div>'; 
  
        } elseif (is_year()){ 
            echo $currentBefore . get_the_time('Y') . $currentAfter; 
			echo '</div>'; 
  
        } elseif (is_single()){ 
            $postType = get_post_type(); 
            if($postType == 'post'){ 
                $cat = get_the_category(); $cat = $cat[0]; 
                echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' '); 
				echo '</div>'; 
            } elseif($postType == 'portfolio'){ 
                $terms = get_the_term_list($post->ID, 'portfolio-category', '', '###', ''); 
                $terms = explode('###', $terms); 
                echo $terms[0]. ' ' . $delimiter . ' '; 
				echo '</div>'; 
            } 
            echo $currentBefore; 
            the_title(); 
            echo $currentAfter; 
  
        } elseif (is_page() && !$post->post_parent){ 
            echo $currentBefore; 
            the_title(); 
            echo $currentAfter; 
			echo '</div>'; 
  
        } elseif (is_page() && $post->post_parent){ 
            $parent_id  = $post->post_parent; 
            $breadcrumbs = array(); 
            while($parent_id){ 
                $page = get_page($parent_id); 
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>'; 
                $parent_id  = $page->post_parent; 
            } 
            $breadcrumbs = array_reverse($breadcrumbs); 
            foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' '; 
            echo $currentBefore; 
            the_title(); 
            echo $currentAfter; 
			echo '</div>'; 
  
        } elseif (is_search()){ 
            echo $currentBefore . __('Search Results for:', 'wpinsite'). ' &quot;' . get_search_query() . '&quot;' . $currentAfter; 
			echo '</div>'; 
  
        } elseif (is_tag()){ 
            echo $currentBefore . __('Post Tagged with:', 'wpinsite'). ' &quot;'; 
            single_tag_title(); 
            echo '&quot;' . $currentAfter; 
			echo '</div>'; 
  
        } elseif (is_author()) { 
            global $author; 
            $userdata = get_userdata($author); 
            echo $currentBefore . __('Author Archive', 'wpinsite') . $currentAfter; 
			echo '</div>'; 
  
        } elseif (is_404()){ 
            echo $currentBefore . __('Page Not Found', 'wpinsite') . $currentAfter; 
			echo '</div>'; 
  
        } 
  
        if(get_query_var('paged')){ 
        if(is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' ('; 
        echo ' ' . $delimiter . ' ' . __('Page') . ' ' . get_query_var('paged'); 
        if(is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')'; 
        } 
  
    } 
} 
?>