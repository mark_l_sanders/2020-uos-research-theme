<?php get_header(); ?>
 <div class="row">
    <div class="col-md-9">
      <!-- WordPress Loop -->
      <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
      <h1 id="post-<?php the_ID(); ?>">
        <?php the_title();?>
      </h1>
      <?php the_content(); ?>
      <?php if ( get_theme_mod( 'uos_research_page_comment_toggle' ) == 1 ) : // show page comments? ?>
      <?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() ) :
						comments_template();
					endif;
				?>
      <?php endif; ?>
      <?php endwhile; ?>
      <?php else : ?>
      <h6 class="center">Not Found</h6>
      <p class="center">Sorry, but you are looking for something that isn't here.</p>
      <?php endif; ?>
      <!-- End WordPress Loop -->
    </div> <!--/.col-md-9-->
    <div class="col-md-3 sidebar">
      <?php if ( is_active_sidebar( 'sidebar-2' ) && is_front_page() ) : ?>
<!-- It IS The front page and a front page sidebar has been defined. -->
      <div id="secondary" class="widget-area" role="complementary">
        <?php dynamic_sidebar( 'sidebar-2' ); ?>
      </div><!--#secondary-->
<?php elseif (is_front_page) : ?>
<!-- It IS The front page but NO front page sidebar has been defined. -->
<div id="secondary" class="widget-area" role="complementary">
        
      </div><!--#secondary-->

<?php elseif ( is_active_sidebar( 'sidebar-1' ) ) : ?>
<!-- It is NOT the front page and a main sidebar has been defined. -->
		  <div id="secondary" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		  </div><!--#secondary-->
      <?php endif; ?>
    </div><!-- /.col-md-3 sidebar -->
    </div><!--/.row-->
<?php get_footer(); ?>
