<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div class="main-content">
 * @package UoS Research Blog
 */
?>
<!DOCTYPE html>
<!-- Start header.php -->
<html <?php language_attributes(); ?>>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<head>
<meta charset="utf-8">
<title>
<?php if(is_front_page()) { echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { wp_title(); ?> -  <?php bloginfo( 'name' );  } ?>
</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="">
<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
<?php wp_head(); ?>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="apple-touch-icon-precomposed" sizes="152&#120;152" href="<?php bloginfo('template_url');?>/img/ico/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon-precomposed" sizes="120&#120;120" href="<?php bloginfo('template_url');?>/img/ico/apple-touch-icon-120x120.png">
 <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-2643712-2', 'salford.ac.uk');
	  ga('create', 'UA-2643712-23',{'name':'blogTracker'});
	  ga('require', 'displayfeatures');
      ga('require', 'linkid', 'linkid.js');
      ga('send', 'pageview');
	  ga('blogTracker.send','pageview');
</script>
    </head>
<body <?php body_class(); ?>>
<?php if ( get_theme_mod( 'uos_research_page_social_icons_toggle' ) == 1 ) : // show social media icons? ?>
<?php get_template_part('/inc/social-media'); ?>
<?php endif; ?>
<section id="header-wrapper">
  <div id="content" class="container">
    <nav class="nav col-md-12" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <div class="logo">
            <?php if ( get_theme_mod( 'uos_research_logo' ) ) : ?>
            <div class='site-logo'> <a href='<?php echo esc_url( home_url( '/' ) ); ?>' class=" pull-left" title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'uos_research_logo' ),'https' ); ?>' alt='<?php bloginfo( 'description' ); ?>'></a>
            <div class="logo-title">
            <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'>
              <?php bloginfo( 'name' ); ?>
              </a></h1>
            <h2 class='site-description'>
              <?php bloginfo( 'description' ); ?>
            </h2>
            </div><!--/.logo-title-->
            </div><!--/.site-logo-->
            <?php else : ?>
            <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'>
              <?php bloginfo( 'name' ); ?>
              </a></h1>
            <h2 class='site-description'>
              <?php bloginfo( 'description' ); ?>
            </h2>
            <?php endif; ?>
          </div>
          <!--/.logo -->
		  <?php if(get_theme_mod('uos_research_university_logo') == 0 || get_theme_mod('uos_research_university_logo') == '' ) : ?>
		  <div class="uni-logo">
			<a href="https://salford.ac.uk" rel="noopener noreferrer"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo/logo.png" alt="University of Salford Logo"></a>
		  </div>
		  <?php endif; ?>
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="primary-navbar-collapse" class="collapse navbar-collapse col-md-12">
          <!-- This shows any menu -->
          <?php
	if (has_nav_menu('header-menu')) {
  wp_nav_menu( array(
        'theme_location' => 'header-menu',
        'menu'       => 'Main menu',
        'depth'      => 3,
        'container'  => false,
        'menu_class' => 'nav navbar-nav',
        'fallback_cb' => 'wp_page_menu',
        //Process nav menu using custom nav walker
       	'walker'	=> new wp_bootstrap_navwalker,
  ) );
}
?>
          <?php
	if (has_nav_menu('secondary')) {
  wp_nav_menu( array(
        'theme_location' => 'secondary',
        'menu'       => 'Secondary menu',
        'depth'      => 1,
        'container'  => false,
        'menu_class' => 'nav navbar-nav visible-xs',
        'fallback_cb' => 'wp_page_menu',
        //Process nav menu using custom nav walker
       	'walker'	=> new wp_bootstrap_navwalker,
  ) );
}
?>
          <form id="expanding-search"  class="navbar-search" action="<?php echo home_url( '/' ); ?>" method="get" role="search">
            <input id="nav-search" name="s" type="nav-search" onfocus="placeholder='Search'" onblur="placeholder=''">
          </form>
        </div>
        <!-- /.navbar-collapse -->

    </nav>
    <?php if ( get_theme_mod( 'uos_research_page_breadcrumb_toggle' ) == 1 ) : // show page comments? ?>
    <div class="row">
      <div class="col-md-12">
        <?php the_breadcrumb(); ?>
      </div>
      <!--/.col-md-12-->
    </div>
    <!--/.row-->
     <?php endif; ?>
  </div>
  <!--/.container-->
</section>
<div class="container" id="content">
<section id="main content">
<?php if ( is_front_page())  {  ?>
<header id="masthead" class="site-header" role="banner">
  <div class="site-branding">
    <?php if ( get_theme_mod( 'uos_research_page_carousel_toggle', 1 ) == 0) : // show homepage carousel ?>
<!-- Should show carousel now -->
    <?php get_template_part('/inc/carousel-branding'); ?>
    <?php else : ?>
    <?php if (get_header_image() != '') :?>
<!-- Should show static image now -->
    <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" class="img-responsive" />
    <?php endif; ?>
    <?php endif; ?>
  </div>
  <!--/.site-branding-->
</header>
<!-- #masthead -->
<?php } elseif ( is_404() ) { ?>
<?php } elseif ( is_search() ) { ?>
<?php } else { ?>
<?php } ?>
<div class="main-content" id="main-content">
  <!-- End header.php -->
